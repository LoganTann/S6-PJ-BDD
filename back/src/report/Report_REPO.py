
def ListCandidateWithMoreThanThreeApplication(db):
    with db.cursor() as c:
        c.execute("""
            SELECT P.Nom, P.Prenom, COUNT(C.Id)
            FROM Personne P
                JOIN Candidature C ON P.Id = C.IdProspect
            GROUP BY P.Id
            HAVING COUNT(C.Id) >= 3
            ORDER BY 3 DESC;
        """)
        return c.fetchall()
    
def listHomesWithoutApplication(db):
    with db.cursor() as c:
        c.execute("""
            SELECT L.Type, L.Adresse, L.SurfaceHabitable, L.Ville, A.TypeAnnonce, A.Prix, A.date
            FROM Logement L
            JOIN Annonce A ON L.Id = A.IdLogement
            LEFT JOIN Candidature C ON A.Id = C.IdAnnonce
            WHERE C.Id IS NULL; 
        """)
        return c.fetchall()

def ListCityOwners(db, ville):
    with db.cursor() as c:
        c.execute("""
            SELECT P.Nom, P.Prenom, L.Type, L.Adresse, L.SurfaceHabitable
            FROM Personne P
            JOIN Logement L ON P.Id = L.IdProprietaire
            WHERE L.Ville = %s
            ORDER BY P.Nom, P.Prenom, L.Type, L.SurfaceHabitable
        """,(ville,))
        return c.fetchall()

def Turnover(db):
    with db.cursor() as c:
        c.execute("""
        SELECT
            (SELECT COUNT(L.Id) FROM Logement L) 'nbLogements',
            (SELECT COUNT(A.Id) FROM Annonce A)  'nbAnnonces',
            (SELECT COUNT(C.Id) FROM Candidature C) 'nbCandidatures',
            (
                SELECT SUM(T.Commission * A.Prix * 0.01)
                FROM Transaction T
                INNER JOIN Candidature C ON T.IdCandidature = C.Id
                INNER JOIN Annonce A ON C.IdAnnonce = A.Id
            ) 'ChiffreAffaire'
        """)
        return c.fetchall()

def announcementWithoutApplication(db):
    with db.cursor() as c:
        c.execute("""
            SELECT A.TypeAnnonce, A.Prix, L.Ville, L.SurfaceHabitable
            FROM Annonce A
            JOIN Logement L ON A.IdLogement = L.Id
            RIGHT JOIN Candidature C ON A.Id = C.IdAnnonce
            WHERE C.Id IS NULL
        """)
        return c.fetchall()

def apartmentMoreExpensiveThanHouses(db):
    with db.cursor() as c:
        c.execute("""
            SELECT DISTINCT A.TypeAnnonce, L.Ville, L.Adresse, L.SurfaceHabitable, L.NombrePieces, A.Prix
            FROM Annonce A
            JOIN Logement L ON A.IdLogement = L.Id
            WHERE L.Type = 'Appartement' AND A.Prix > ANY (
                SELECT A.Prix
                FROM Annonce A
                JOIN Logement L ON A.IdLogement = L.Id
                WHERE L.Type = 'Maison' AND A.TypeAnnonce = 'V'
            )
            ORDER BY A.Prix DESC
        """)
        return c.fetchall()
    

def memberNotOwner(db):
    with db.cursor() as c:
        c.execute("""
            SELECT P.Nom, P.Prenom
            FROM Personne P
            WHERE NOT EXISTS(
                SELECT 1
                FROM Logement L
                WHERE L.IdProprietaire = P.Id
            )
            ORDER BY 1, 2
        """)
        return c.fetchall()