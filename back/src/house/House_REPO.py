  

def showCityStatsForHouse(db, idAnnonce):
    with db.cursor() as c:
        c.execute("""
            SELECT A1.Prix, MAX(A2.Prix), MIN(A2.Prix), AVG(A2.Prix)
            FROM Annonce A1, Annonce A2, Logement L1, Logement L2
            WHERE A1.Id = %s
                AND A1.IdLogement = L1.Id
                AND L1.Ville = L2.Ville
                AND A2.IdLogement = L2.Id
        """, (idAnnonce,))
        return c.fetchall()



