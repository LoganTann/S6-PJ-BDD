from src.house import House_REPO


def showHouseDetails(db, idAnnonce):
    return {"error": "TODO : showHouseDetails"}

def showCityStatsForHouse(db, idAnnonce):
    rows = House_REPO.showCityStatsForHouse(db, idAnnonce)
    firstRow = rows[0]
    return {
        "prixLogement": firstRow[0],
        "max": firstRow[1],
        "min": firstRow[2],
        "average": firstRow[3]
    }
