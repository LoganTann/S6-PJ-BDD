from src.owner.Owner_REPO import ListHousesByOwner

def listHousesByOwner(db, idPersonne):
    result = []
    for row in ListHousesByOwner(db, idPersonne):
        result.append({
            "idAnnonce": row[0],
            "image": row[1],
            "typeAnnonce": "Vente" if row[2] == "V" else "Location",
            "adresse": row[3],
            "ville": row[4],
            "nbCand": row[5],
            "nbPieces": row[6],
            "surface": row[7],
            "etat": row[8]
        })
    return result
