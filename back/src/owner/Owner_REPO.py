
def ListHousesByOwner(db, idPersonne):
    with db.cursor() as c:
        c.execute("""
            SELECT A.Id, A.Image, L.Type, L.Adresse, L.Ville, -1,
                L.NombrePieces, L.SurfaceHabitable, L.Etat
            FROM Logement L
                LEFT JOIN Annonce A ON L.Id = A.IdLogement
            WHERE L.IdProprietaire = %s
            ORDER BY L.Id
        """, (idPersonne,))
        return c.fetchall()
