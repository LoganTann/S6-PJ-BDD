from src.search import Search_REPO;

def listHomes(db, params):
    result = []
    for row in Search_REPO.listHomes(db, params):
        result.append({
            "id": row[0],
            "typeLogement": row[1],
            "nombrePieces": row[2],
            "surfaceHabitable": row[3],
            "ville": row[4],
            "image": row[5],
            "typeAnnonce": "Vente" if row[6] == "V" else "Location",
            "prix": row[7],
            "date": row[8].strftime("%d/%m/%Y")
       })
    return result

def listCities(db, cityInput):
    result = []
    for row in Search_REPO.listCities(db, cityInput):
        result.append({"value": row[0]})
    return result