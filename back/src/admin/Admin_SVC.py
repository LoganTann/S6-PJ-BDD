from src.admin import Admin_REPO

def showApplicationDetails(db, idApplication):
    result = Admin_REPO.showApplicationDetails(db, idApplication)
    row = result[0]
    return {
        "image": row[0],
        "typeLogement": row[1],
        "adresse": row[2],
        "surface": row[3],
        "prix": row[4],
        "nomProprio": row[5],
        "prenomProprio": row[6],
        "dateVisite": None if row[7] == None else row[7].strftime("%d/%m/%Y"),
        "etatVisite": row[8],
        "commission": row[9],
        "nomCandidat": row[10],
        "prenomCandidat": row[11],
    }

def showAllApplication(db, params):
    result = []
    if params["idAnnonce"] == 0:
        for row in Admin_REPO.showApplicationsListWithCount(db, params):
            result.append({
                "idAnnonce": row[0],
                "image": row[1],
                "typeAnnonce": "Vente" if row[2] == "V" else "Location",
                "adresse": row[3],
                "ville": row[4],
                "nbCand": row[5]
            })
        return result
    for row in Admin_REPO.showAllApplicationOfAnnouncement(db, params):
        result.append({
            "nom": row[0],
            "prenom": row[1],
            "applicationId": row[2]
        })
    return result

def listVisitRequests(db):
    result = []
    for row in Admin_REPO.listVisitRequests(db):
        result.append({
            "nom": row[0],
            "prenom": row[1],
            "date": row[2].strftime("%d/%m/%Y"),
            "etat": row[3],
            "adresse": row[4],
            "ville": row[5],
            "applicationId": row[6]
        })
    return result