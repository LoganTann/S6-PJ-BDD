
def showApplicationDetails(db, idApplication):
    with db.cursor() as c:
        c.execute("""
            SELECT A.Image, L.Type, L.Adresse, L.SurfaceHabitable, A.Prix,
                    P.Nom, P.Prenom,
                    V.Date, V.Etat,
                    T.Commission,
                    P2.Nom, P2.Prenom
            FROM Logement L
                INNER JOIN Personne P ON L.IdProprietaire = P.Id
                INNER JOIN Annonce A ON A.IdLogement = L.Id
                INNER JOIN Candidature C ON C.IdAnnonce = A.Id
                INNER JOIN Personne P2 ON P2.Id = C.IdProspect
                    LEFT JOIN Visite V ON V.IdCandidature = C.Id
                    LEFT JOIN Transaction T ON T.IdCandidature = C.Id
            WHERE C.Id = %s
        """, (idApplication,))
        return c.fetchall()

def showAllApplicationOfAnnouncement(db, sqlParams):
    with db.cursor() as c:
        query = """
            SELECT DISTINCT P.Nom, P.Prenom, C.Id
            FROM Personne P
            JOIN Candidature C ON P.Id = C.IdProspect
            JOIN Annonce A ON C.IdAnnonce = A.Id
            WHERE IdAnnonce = %s
        """
        execParams=(
            sqlParams["idAnnonce"],
        )
        c.execute(query, execParams)
        return c.fetchall()
    
def showApplicationsListWithCount(db, sqlParams):
    with db.cursor() as c:
        query = """
            SELECT A.Id, A.Image, A.TypeAnnonce, L.Adresse, L.Ville, COUNT(C.Id)
            FROM Candidature C
            JOIN Annonce A ON C.IdAnnonce = A.Id
            JOIN Logement L ON A.IdLogement = L.Id
            GROUP BY A.Id
            ORDER BY L.Ville, COUNT(C.Id) DESC
        """
        c.execute(query)
        return c.fetchall()

def listVisitRequests(db):
    with db.cursor() as c:
        c.execute("""
            SELECT P.Nom, P.Prenom, V.Date, V.Etat, L.Adresse, L.Ville, C.Id
            FROM Visite V
            INNER JOIN Candidature C ON V.IdCandidature = C.Id
            INNER JOIN Personne P ON C.IdProspect = P.Id
            INNER JOIN Annonce A ON C.IdAnnonce = A.Id
            INNER JOIN Logement L ON A.IdLogement = L.Id
        """)
        return c.fetchall()
    
def editEtatVisite(db, id, etat):
    with db.cursor() as c:
        c.execute("""
        UPDATE `Visite` SET
            `Etat` = %s
            WHERE `Id` = %s;
        """, (etat, id))
        return c.fetchall()
    
def insertTransaction(db, idCandidature, commission):
    with db.cursor() as c:
        c.execute("""
        INSERT INTO `Transaction` (`IdCandidature`, `Commission`)
            VALUES (%s, %s);
        """, (idCandidature, commission))
        return c.fetchall()
    
