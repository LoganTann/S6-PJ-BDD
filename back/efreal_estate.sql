-- Adminer 4.8.1 MySQL 8.0.32 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `Annonce`;
CREATE TABLE `Annonce` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `IdLogement` int NOT NULL,
  `TypeAnnonce` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Prix` decimal(10,0) NOT NULL,
  `Date` date NOT NULL,
  `Actif` tinyint NOT NULL,
  `Image` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdLogement` (`IdLogement`),
  CONSTRAINT `Annonce_ibfk_1` FOREIGN KEY (`IdLogement`) REFERENCES `Logement` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `Annonce` (`Id`, `IdLogement`, `TypeAnnonce`, `Prix`, `Date`, `Actif`, `Image`) VALUES
(1,	1,	'L',	800,	'2023-04-01',	1,	'/images/1_appartement.png'),
(2,	2,	'L',	800,	'2023-04-01',	1,	'/images/2_appartement.png'),
(3,	3,	'L',	900,	'2023-04-01',	1,	'/images/3_maison.png'),
(4,	4,	'L',	850,	'2023-04-01',	1,	'/images/4_appartement.png'),
(5,	5,	'L',	750,	'2023-04-01',	1,	'/images/5_maison.png'),
(6,	6,	'L',	1000,	'2023-04-01',	1,	'/images/6_maisons.png'),
(7,	7,	'L',	1600,	'2023-04-01',	1,	'/images/7_appartement.png'),
(8,	8,	'L',	750,	'2023-04-01',	1,	'/images/8_maison.png'),
(9,	9,	'L',	1550,	'2023-04-01',	1,	'/images/9_appartement.png'),
(10,	10,	'L',	865,	'2023-04-01',	1,	'/images/10_maison.png'),
(11,	11,	'L',	2230,	'2023-04-01',	1,	'/images/11_appartement.png'),
(12,	12,	'L',	550,	'2023-04-01',	1,	'/images/12_maison.png'),
(13,	13,	'L',	1200,	'2023-04-01',	1,	'/images/13_appartement.png'),
(14,	14,	'V',	45000,	'2023-04-01',	1,	'/images/14_maison.png'),
(15,	15,	'V',	210000,	'2023-04-01',	1,	'/images/15_appartement.png'),
(16,	16,	'V',	875000,	'2023-04-01',	1,	'/images/16_maison.png'),
(17,	17,	'V',	195000,	'2023-04-01',	1,	'/images/17_appartement.png'),
(18,	18,	'V',	725000,	'2023-04-01',	1,	'/images/18_appartement.png');

DROP TABLE IF EXISTS `Candidature`;
CREATE TABLE `Candidature` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `IdAnnonce` int NOT NULL,
  `IdProspect` int NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdAnnonce` (`IdAnnonce`),
  KEY `IdProspect` (`IdProspect`),
  CONSTRAINT `Candidature_ibfk_1` FOREIGN KEY (`IdAnnonce`) REFERENCES `Annonce` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `Candidature_ibfk_2` FOREIGN KEY (`IdProspect`) REFERENCES `Personne` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `Candidature` (`Id`, `IdAnnonce`, `IdProspect`) VALUES
(1,	2,	21),
(2,	2,	23),
(3,	5,	21),
(4,	6,	21),
(5,	7,	21),
(6,	1,	20),
(7,	16,	24),
(10,	1,	24),
(11,	1,	25),
(12,	1,	25),
(13,	10,	24),
(14,	10,	29),
(15,	10,	30),
(16,	11,	29),
(17,	13,	29),
(18,	14,	29),
(19,	15,	22),
(20,	15,	25),
(21,	16,	27),
(22,	17,	27),
(23,	18,	28),
(25,	2,	22),
(27,	2,	24),
(28,	3,	23),
(29,	3,	29),
(30,	4,	24),
(31,	4,	23),
(32,	4,	28),
(33,	5,	29),
(34,	9,	23),
(35,	9,	25),
(36,	9,	26);

DROP TABLE IF EXISTS `Logement`;
CREATE TABLE `Logement` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Type` varchar(20) NOT NULL,
  `Adresse` text NOT NULL,
  `NombrePieces` int NOT NULL,
  `NombreGarages` int NOT NULL,
  `SurfaceHabitable` decimal(10,0) NOT NULL,
  `Etat` varchar(20) NOT NULL,
  `Ville` varchar(60) NOT NULL,
  `IdProprietaire` int NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Proprietaire` (`IdProprietaire`),
  CONSTRAINT `Logement_ibfk_1` FOREIGN KEY (`IdProprietaire`) REFERENCES `Personne` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `Logement` (`Id`, `Type`, `Adresse`, `NombrePieces`, `NombreGarages`, `SurfaceHabitable`, `Etat`, `Ville`, `IdProprietaire`) VALUES
(1,	'Appartement',	'1 Rue de la Paix',	2,	1,	20,	'bien',	'Paris',	1),
(2,	'Appartement',	'1 Rue de la Paix',	1,	0,	40,	'bien',	'Paris',	1),
(3,	'Maison',	'3 Rue de Rivoli',	0,	0,	20,	'correct',	'Paris',	2),
(4,	'Appartement',	'5 Avenue Montaigne',	0,	0,	25,	'bien',	'Paris',	3),
(5,	'Maison',	'2 Place du Trocadéro et du 11 Novembre',	6,	2,	20,	'bien',	'Paris',	4),
(6,	'Maison',	'7 Rue Saint-Jacques',	2,	0,	35,	'Neuf',	'Paris',	5),
(7,	'Appartement',	'12 Rue de la Pompe',	5,	0,	120,	'bien',	'Paris',	6),
(8,	'Maison',	'17 Avenue George V',	2,	0,	20,	'correct',	'Paris',	7),
(9,	'Appartement',	'19 Rue de la Paix',	2,	0,	130,	'bien',	'Paris',	9),
(10,	'Maison',	'25 Rue Royale',	2,	0,	18,	'correct',	'Paris',	10),
(11,	'Appartement',	'31 Rue Cambon',	4,	0,	180,	'neuf',	'Paris',	11),
(12,	'Maison',	'1 Rue de la Fontaine',	2,	1,	30,	'neuf',	'Cannes',	12),
(13,	'Appartement',	'23 Avenue des Baumettes',	3,	1,	20,	'neuf',	'Nice',	8),
(14,	'Maison',	'2 Rue de la Citadelle',	2,	1,	22,	'bien',	'Carcassonne',	13),
(15,	'Appartement',	'7 Rue des Belges',	2,	1,	160,	'bien',	'Lyon',	14),
(16,	'Maison',	'15 Rue du Faubourg Saint-Honoré',	1,	1,	20,	'bien',	'Paris',	15),
(17,	'Appartement',	'4 Rue de la Paix',	6,	1,	120,	'bien',	'Marseille',	16),
(18,	'Appartement',	'22 Rue de la République',	2,	40,	1,	'bien',	'Lyon',	17),
(19,	'Maison',	'9 Rue de la Boétie',	4,	1,	120,	'bien',	'Paris',	18),
(20,	'Appartement',	'39 Avenue des Ternes',	2,	1,	20,	'bien',	'Paris',	1),
(21,	'Appartement',	'4 Place Vendôme',	2,	1,	20,	'bien',	'Paris',	1);

DROP TABLE IF EXISTS `Personne`;
CREATE TABLE `Personne` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Nom` varchar(150) NOT NULL,
  `Prenom` varchar(150) NOT NULL,
  `Adresse` text NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `Personne` (`Id`, `Nom`, `Prenom`, `Adresse`) VALUES
(1,	'TANN',	'Logan',	'à efrei 1'),
(2,	'RIOU',	'Anaëlle',	'à efrei 2'),
(3,	'Dupont',	'Jean',	'1 Rue de la Paix'),
(4,	'Durand',	'Marie',	'10 Avenue des Champs-Élysées'),
(5,	'Dubois',	'Pierre',	'3 Rue de Rivoli'),
(6,	'Martin',	'Sophie',	'5 Avenue Montaigne'),
(7,	'Moreau',	'Thomas',	'2 Place du Trocadéro et du 11 Novembre'),
(8,	'Lefèvre',	'Camille',	'7 Rue Saint-Jacques'),
(9,	'Mercier',	'Jacques',	'12 Rue de la Pompe'),
(10,	'Rousseau',	'Charlotte',	'17 Avenue George V'),
(11,	'Laurent',	'Vincent',	'19 Rue de la Paix'),
(12,	'Girard',	'Isabelle',	'25 Rue Royale'),
(13,	'Roux',	'Guillaume',	'31 Rue Cambon'),
(14,	'Chevalier',	'Emilie',	'1 Rue de la Fontaine'),
(15,	'Picard',	'Julien',	'23 Avenue des Baumettes'),
(16,	'Leclerc',	'Amélie',	'2 Rue de la Citadelle'),
(17,	'Dumas',	'Mathieu',	'7 Rue des Belges'),
(18,	'Levesque',	'Céline',	'15 Rue du Faubourg Saint-Honoré'),
(19,	'Gauthier',	'Sébastien',	'4 Rue de la Paix'),
(20,	'Boucher',	'Florence',	'22 Rue de la République'),
(21,	'Deschamps',	'Nicolas',	'9 Rue de la Boétie'),
(22,	'Morel',	'Elodie',	'39 Avenue des Ternes'),
(23,	'Lacroix',	'Théo',	'4 Place Vendôme'),
(24,	'Dupuis',	'Laetitia',	'18 Rue du Faubourg Saint-Antoine'),
(25,	'Delorme',	'Rémi',	'5 Rue Royale'),
(26,	'Lemoine',	'Claire',	'6 Rue Saint-Paul'),
(27,	'Fournier',	'Baptiste',	'3 Rue Royale'),
(28,	'Dubois',	'Léa',	'19 Rue du Bac'),
(29,	'Marchand',	'Benoît',	'28 Rue Saint-Guillaume'),
(30,	'Dupuy',	'Margaux',	'2 Rue de l\'Abbaye');

DROP TABLE IF EXISTS `Transaction`;
CREATE TABLE `Transaction` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `IdCandidature` int NOT NULL,
  `Commission` decimal(10,0) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdCandidature` (`IdCandidature`),
  CONSTRAINT `Transaction_ibfk_1` FOREIGN KEY (`IdCandidature`) REFERENCES `Candidature` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `Transaction` (`Id`, `IdCandidature`, `Commission`) VALUES
(1,	1,	2),
(2,	7,	5),
(3,	3,	2),
(4,	4,	3);

DROP TABLE IF EXISTS `Visite`;
CREATE TABLE `Visite` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `IdCandidature` int NOT NULL,
  `Date` date NOT NULL,
  `Etat` varchar(20) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdCandidature` (`IdCandidature`),
  CONSTRAINT `Visite_ibfk_1` FOREIGN KEY (`IdCandidature`) REFERENCES `Candidature` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `Visite` (`Id`, `IdCandidature`, `Date`, `Etat`) VALUES
(1,	1,	'2023-04-01',	'pas ok'),
(2,	2,	'2023-03-11',	'pas ok'),
(3,	3,	'2023-03-23',	'pas ok'),
(4,	4,	'2023-03-17',	'ok'),
(5,	5,	'2023-03-24',	'pas ok'),
(6,	6,	'2023-03-05',	'ok'),
(7,	7,	'2023-03-14',	'ok'),
(10,	10,	'2023-03-28',	'ok');

-- 2023-02-27 07:27:14

