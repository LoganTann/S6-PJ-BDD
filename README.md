<div align="center">

![Efreal-estate](https://user-images.githubusercontent.com/28659185/221782795-d7983062-e135-40e2-8968-74d8cbe9dc0f.svg)

**Projet Base de Données S6**

</div>

## Installation du back (développement)

Ce projet requiert docker et docker-compose pour télécharger et lancer les différents services.

Une fois ceci fait, il faut créer les images docker :

```powershell
# Télécharge les dépendances du back-end, et empaquete le projet dans une image docker
$ cd back
$ docker build -t efreal-back .
```

Enfin, pour démarrer les services :

```powershell
$ docker compose up -d
```

Les variables d'environnement du docker-compose et le script de datasetup dans app.py devraient permettre d'initialiser sans problème la base de données.

## Installation du front (développement)

Le front n'est pas dockeurisé (de toute façon, démarrer vite en natif reste adapté pour du développement)

```powershell
# Télécharge les dépendances du front-end
$ cd front
$ npm install # ou `yarn`
```

Enfin, pour démarrer le serveur de développement :

```powershell
$ npm run dev # ou `yarn dev`

```

## Autre

Utiliser `docker compose down -v` pour supprimer les volumes.
